from pdb import set_trace
from collections import OrderedDict

import sys
import sh
import re
import os

class verbosity:
    DEFAULT = 0
    MINIMAL = 1
    MOST    = 2
    ALL     = 3


class profile_t:
    def __init__(self):
        self.name = None
        self.cc = "gcc"
        self.cc_asm = None
        self.cxx = "g++"
        self.ld = "ld"
        self.objcopy = "objcopy"
        self.objdump = "objdump"
        self.include_dirs = None
        self.source_dirs = None
        self.source_files = None
        self.cflags = []
        self.cxxflags = []
        self.ldflags = []
        self.linker_scripts = []
        self.libraries = []
        self.arflags = None
        self.pre_commands = OrderedDict()
        self.executable = None
        self.executable_subtargets = OrderedDict()
        self.library = None
        self.verbose_level = None
        self.build_dir = None
        self.additional_commands = OrderedDict()
        self.file_types = [".c", ".cxx", ".cpp", ".s", ".asm"]

def get_build_dir(profile):
    return profile.build_dir + "/" + profile.name + "/"

def delete_build_dir(profile):
    pass

def create_build_dir(profile):
    build_dir = get_build_dir(profile)
    sh.mkdir(build_dir, "-p")

def build(profiles_list, args):
    profiles = OrderedDict()
    for profile in profiles_list:
        profiles[profile.name] = profile
    
    try:
        profile = profiles[args[0]]
    except:
        print("Unknown profile to build")
        sys.exit(0)
    profile_command = None
    try:
        profile_command = args[1]
    except:
        if len(args)==1:
            print("Profile command not specified")
            sys.exit(0)
        else:
            print("Unknown error")
            sys.exit(0)
    if profile_command=="clean":
        clean_profile(profile)
    elif profile_command=="build":
        if len(profile.pre_commands):
            # Execute the pre-commands before building the executable or library
            for (key, value) in  profile.pre_commands.items():
                try:
                    exec_cmd(sh.Command(value[0]), value[1:], None)
                except:
                    sys.exit(0);
        if profile.executable:
            build_executable(profile)
        elif profile.library:
            build_library(profile)
        else:
            print("Unknown build setting")
            sys.exit(0)
    else:
        try:
            value = profile.additional_commands[profile_command]
        except:
            print("Unknown build command")
            sys.exit(0)
        try:
            exec_cmd(sh.Command(value[0]), value[1:], None) 
        except:
            sys.exit(0)
        

def std_err(line, stdin, process):
    print(line,end='',flush=True)

def exec_cmd(cmd, cmd_args, verbose_level):
    cmd_name = cmd.__name__.split("/")[-1]
    print("{0} {1}".format(cmd_name, " ".join(cmd_args)))
    cmd(cmd_args, _out=std_err, _err=std_err)

def clean_profile(profile):
    exec_cmd(sh.rm, ["-rf", get_build_dir(profile)], None)

def link_objects(profile, objs):
    ld = sh.Command(profile.ld)
    ldflags = profile.ldflags
    linker_scripts = [ "-T" + linker_script for linker_script in profile.linker_scripts]
    libraries = profile.libraries
    target = get_build_dir(profile) + profile.executable
    try:
        args = objs + linker_scripts + ldflags + libraries + [("-o")] + [(target)]
        exec_cmd(ld, args, profile.verbose_level)
    except:
        set_trace()
        sys.exit(0)

def compile_c_sources(profile, includes, c_srcs):
    objs = list()
    cc = sh.Command(profile.cc)
    cflags = profile.cflags
    incls  = ["-I" + inc for inc in includes]
    for c_file in c_srcs:
        obj = get_build_dir(profile) + re.sub("\..*$", ".o", c_file.split("/")[-1])
        try:
            args = cflags + incls + [("-c")] + [(c_file)] + [("-o")] + [(obj)]
            exec_cmd(cc, args, profile.verbose_level)
            objs += [obj]
        except:
            set_trace()
            sys.exit(0)
    return objs

def compile_cxx_sources(profile, includes, cxx_srcs):
    objs = list()
    cxx = sh.Command(profile.cxx)
    cxxflags = profile.cxxflags
    incls  = ["-I" + inc for inc in includes]
    for cxx_file in cxx_srcs:
        obj = get_build_dir(profile) + re.sub("\..*$", ".o", cxx_file.split("/")[-1])
        try:
            args = cxxflags + incls + [("-c")] + [(cxx_file)] + [("-o")] + [(obj)]
            exec_cmd(cxx, args, profile.verbose_level)
            objs += [obj]
        except:
            set_trace()
            sys.exit(0)
    return objs

def compile_asm_sources(profile, includes, asm_srcs):
    objs = list()
    asm = sh.Command(profile.cc_asm)
    incls = ["-I" + inc for inc in includes]
    for asm_file in asm_srcs:
        obj = get_build_dir(profile) + re.sub("\..*$", ".o", asm_file.split("/")[-1])
        args = incls + [("-c")] + [(asm_file)] + [("-o")] + [(obj)]
        exec_cmd(asm, args, profile.verbose_level)
        objs += [obj]
    return objs

def build_executable(profile):
    includes = list()
    c_srcs = list()
    cxx_srcs = list()
    asm_srcs = list()
    if profile.source_files==None:
        all_srcs = list()
    else:
        all_srcs = profile.source_files
    for directory in profile.source_dirs:
        for f in os.listdir(directory):
            if f.endswith(tuple(profile.file_types)):
                try:
                    all_srcs += [directory + "/" + f]
                except:
                    set_trace()
                    pass

    if profile.include_dirs:
        includes += profile.include_dirs

    if len(all_srcs):
        for source in all_srcs:
            if re.search("\.c$", source):
                c_srcs += [source]
            elif re.search("\.cpp$", source) or re.search("\.cxx$", source):
                cxx_srcs += [source]
            elif re.search("\.s$", source) or re.search("\.asm$", source) :
                asm_srcs += [source]
            else:
                print("Unknown source file specified")
                sys.exit(0)
    objs = list()
    create_build_dir(profile)
    objs += compile_c_sources(profile, includes, c_srcs)
    objs += compile_cxx_sources(profile, includes, cxx_srcs)
    objs += compile_asm_sources(profile, includes, asm_srcs)
    link_objects(profile, objs)
    for key in profile.executable_subtargets.keys():
        value = profile.executable_subtargets[key]
        exec_cmd(sh.Command(value[0]), value[1:], None) 

def build_library(profile):
    pass
