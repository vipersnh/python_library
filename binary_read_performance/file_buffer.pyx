from libc.stdio cimport *                                                                
from libc.stdlib cimport malloc, free
from libc.stdint cimport *

#cdef extern from "stdio.h":
#    FILE *fopen(const char *, const char *)
#    int fclose(FILE *)
#    ssize_t getline(char **, size_t *, FILE *)


cdef class file_buffer_t:
    cdef FILE* bin_file
    cdef uint32_t* bytes_buffer
    def __init__(self,bin_file_name, max_buf_size):
        fname = bin_file_name.encode('utf-8')
        self.max_buf_size = max_buf_size
        self.bin_file_name    = bin_file_name
        self.bytes_buffer = <uint32_t*>malloc(max_buf_size)
        self.bin_file = fopen(fname, "rb")

    
    def get_next_nbytes(self, nbytes):
        cdef uint32_t temp
        temp = fread(<void*>self.bytes_buffer, nbytes, 1, self.bin_file)
        return self.bytes_buffer
